// .....................................................................................................................................................................
// read confguration from settings.js
var settings = require('../settings.js');

var elasticsearch=require('elasticsearch');

var client = new elasticsearch.Client( {  
  hosts: [
    'http://'+settings.elastic_user+':'+settings.elastic_password+'@'+settings.elastic_host+':'+settings.elastic_port+'/'
  ]
});

module.exports = client;  
console.log ("connected to Elasticsearch.")