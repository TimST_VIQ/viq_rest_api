var client = require('./connection.js');

client.search({  
  index: 'products',
  type: '7',
  body: {
    query: {
      match: { "product_id": "123456" }
    },
  }
},function (error, response,status) {
    if (error){
      console.log("search error: "+error)
    }
    else {
      console.log("--- Response ---");
      console.log(response);
      console.log("--- Hits ---");
      response.hits.hits.forEach(function(hit){
        console.log(hit);
      })
    }
});