// .....................................................................................................................................................................
// created by Tim Steinlein on 2017-03-28
// purpose: provide a rest api to pass on search requests to elastic search cluster
// .....................................................................................................................................................................



// .....................................................................................................................................................................
// read confguration from settings.js
var settings = require('./settings.js');


//required modules
// .....................................................................................................................................................................
var express = require('express');
var app = express();
var http = require('http');
var https = require('https');
var fs = require("fs");
var bodyParser = require('body-parser');
var app = require('express')()
app.use(express.static('public'));


app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

    //intercepts OPTIONS method
    if ('OPTIONS' === req.method) {
      //respond with 200
      res.sendStatus(200);
    }
    else {
    //move on
      next();
    }
});



// .....................................................................................................................................................................
//  index page
app.get('/', function (req, res) {
   console.log("Got a GET request for the api documentation");
   app.use(express.static('public'));

//app.get('/index.html', function (req, res) {
   res.sendFile( __dirname + "/" + "index.html" );
// })
   
})



// .....................................................................................................................................................................
//  version
app.get('/version', function (req, res) {
   console.log("Show API Version");
   
   res.send("API VERSION 0.0.3");
})



// .....................................................................................................................................................................
//  authentification

var basicAuth = require('express-basic-auth')
 
app.use(basicAuth({
    users: { 'api_user' : 'api_password' },
  //  users: { 'api_user': 'admin'},
    
    unauthorizedResponse: getUnauthorizedResponse,
    challenge: true,
    realm: 'Imb4T3st4pp'
}))




function getUnauthorizedResponse(req) {
    return req.auth ?
        ('Credentials ' + req.auth.user + ':' + req.auth.password + ' rejected') :
        'No credentials provided'
        console.log('no credentials provided')
}


// .....................................................................................................................................................................
//  connect to Elasticsearch
var client = require('./elasticsearch/connection.js');

// .....................................................................................................................................................................
//  show index page



app.get('/get_product_count/:tenant_id', function (req, res) {
   console.log("Got a GET request for /get_product_count/"+req.params.tenant_id);
   
   client.count({index: 'products',type: req.params.tenant_id },function(err,resp,status) {  
  var count_result= JSON.parse( JSON.stringify(resp) );
  console.log("count for tenant id "+req.params.tenant_id+" : " +count_result.count);
     res.send("get_product_count for tenant_id "+req.params.tenant_id+" : " +count_result.count );
   
   });
})



// This responds a GET request for the /get_product_by_id
app.get('/get_product_by_id/:tenant_id/:id', function (req, res) {
  // console.log("Got a GET request for /get_product_by_id/"+req.params.tenant_id+"/"+req.params.id);
  
  client.search({  
  index: 'products',
  type: req.params.tenant_id,
  body: {
    query: {
      match: { "product_id": req.params.id }
    },
  }
},function (error, response,status) {
    if (error){
      console.log("search error: "+error)
        res.send(error)
    }
    else {
  //    console.log("--- Response ---");
  //  console.log(response);
  //    console.log("--- Hits ---:"+response.hits.total);
  
  // when product not found return "not found"
  if(response.hits.total > 0) {
         
            response.hits.hits.forEach(function(hit){
                var product_out=JSON.parse( JSON.stringify(hit) )
      //           console.log('get_product_by_id: '+ req.params.id);
                res.send(product_out._source);
            })
            }
            else
            {
              res.send('Product Id '+ req.params.id+ ' does not exist.');   
                }
            
    }
}); 
       
})



/*
// This responds a GET request for the /get_product_by_searchterm
app.get('/get_product/:tenant_id/:searchterm', function (req, res) {
   console.log("Got a GET request for /get_product for tenant_id "+req.params.tenant_id+" with searchterm: "+req.params.searchterm+" and fields: "+settings.get_product_fields_included_in_search);
  // var searchterm=req.params.searchterm
//   res.send('get_product: searchterm is '+req.query.searchterm);

if (req.params.searchterm != null) {      
   
    client.search({  
  index: 'products',
  type: req.params.tenant_id,
  body: {
    query: {
       "multi_match": {
      "query": req.params.searchterm,
      "fields": settings.get_product_fields_included_in_search , // only fields that are not a number
      "type": "phrase"
                     }
      
        
           },
        }
},function (error, response,status) {
    if (error){
      console.log("search error: "+error)
        res.send(error)
    }
    else {
  //    console.log("--- Response ---");
  //    console.log(response);
  //    console.log("--- Hits ---");

         console.log('Searchterm: '+ req.params.searchterm);
         
         
          if(response.hits.total > 0) {
         
              //   console.log(response.hits.hits._source)
                          
                     var product_out_main=JSON.parse( JSON.stringify(response.hits.hits) )
                            
                     res.send(product_out_main);

                     console.log(product_out_main)
            }
            else
            {
                res.send("No results found for searchterm: "+req.params.searchterm); 
                }
            
     // response.hits.hits.forEach(function(hit){
    //   var product_out=JSON.parse( JSON.stringify(hit) )
        
   //       console.log('Searchterm: '+ req.query.searchterm);
        
     //   res.send(product_out._source);
    //  })

        
          
    }
}); 
       
}
else
{
 res.send("error - no searchterm provided.  ") 
    }   
   
})

*/





// This responds a GET request for the /get_product_by_searchterm
app.get('/get_product/:tenant_id/:searchterm', function (req, res) {
   // console.log("Got a GET request for /get_product_q for tenant_id "+req.params.tenant_id+" with searchterm: "+req.params.searchterm);
  // var searchterm=req.params.searchterm
//   res.send('get_product: searchterm is '+req.query.searchterm);

if (req.params.searchterm != null) {      
   
    client.search({  
  index: 'products',
  type: req.params.tenant_id,
  q:req.params.searchterm+"*",
  size: settings.elastic_nr_rows_returned
// , stored_fields: settings.get_product_fields_included_in_search
, _source: settings.get_product_fields_included_in_search
   
},function (error, response,status) {
    if (error){
      console.log("search error: "+error)
        res.send(error)
    }
    else {
  //    console.log("--- Response ---");
  //    console.log(response);
  //    console.log("--- Hits ---");

    //      console.log('Searchterm: '+ req.params.searchterm);
         
         
          if(response.hits.total > 0) {
         
              //   console.log(response.hits.hits._source)
                          
                     var product_out_main=JSON.parse( JSON.stringify(response.hits.hits) )
                            
                     res.send(product_out_main);

     //                 console.log(product_out_main)
            }
            else
            {
                res.send("No results found for searchterm: "+req.params.searchterm); 
                }
            
     // response.hits.hits.forEach(function(hit){
    //   var product_out=JSON.parse( JSON.stringify(hit) )
        
   //       console.log('Searchterm: '+ req.query.searchterm);
        
     //   res.send(product_out._source);
    //  })

        
          
    }
}); 
       
}
else
{
 res.send("error - no searchterm provided.  ") 
    }   
   
})



app.get('/get_user_count/:tenant_id', function (req, res) {
   console.log("Got a GET request for /get_user_count/"+req.params.tenant_id);
   
   client.count({index: 'users',type: req.params.tenant_id },function(err,resp,status) {  
  var count_result= JSON.parse( JSON.stringify(resp) );
  console.log("count for tenant id "+req.params.tenant_id+" : " +count_result.count);
     res.send("get_user_count for tenant_id "+req.params.tenant_id+" : " +count_result.count );
   
   });
})
   

// This responds a GET request for the /get_product_by_id
app.get('/get_user_by_id/:tenant_id/:id', function (req, res) {
   console.log("Got a GET request for /get_user_by_id/"+req.params.tenant_id+"/"+req.params.id);
  
  client.search({  
  index: 'users',
  type: req.params.tenant_id,
  body: {
    query: {
      match: { "user_id": req.params.id }
    },
  }
},function (error, response,status) {
    if (error){
      console.log("search error: "+error)
        res.send(error)
    }
    else {
  //    console.log("--- Response ---");
   console.log(response);
  //    console.log("--- Hits ---:"+response.hits.total);
  
  // when product not found return "not found"
  if(response.hits.total > 0) {
         
            response.hits.hits.forEach(function(hit){
                var product_out=JSON.parse( JSON.stringify(hit) )
                console.log('get_user_by_id: '+ req.params.id);
                res.send(product_out._source);
            })
            }
            else
            {
              res.send('User Id '+ req.params.id+ ' does not exist.');   
                }
            
    }
}); 
       
})      
            
                  
// ####################                              
   

// This responds a GET request for the /get_product_by_searchterm
app.get('/get_user/:tenant_id/:searchterm', function (req, res) {
   console.log("Got a GET request for /get_user for tenant_id "+req.params.tenant_id+" with searchterm: "+req.params.searchterm);
  // var searchterm=req.params.searchterm
//   res.send('get_product: searchterm is '+req.query.searchterm);

if (req.params.searchterm != null) {      
   
    client.search({  
  index: 'users',
  type: req.params.tenant_id,
  q:req.params.searchterm,
  size: settings.elastic_nr_rows_returned,
  _source: settings.get_user_fields_included_in_search
   
},function (error, response,status) {
    if (error){
      console.log("search error: "+error)
        res.send(error)
    }
    else {
  //    console.log("--- Response ---");
  //    console.log(response);
  //    console.log("--- Hits ---");

         console.log('Searchterm: '+ req.params.searchterm);
         
         
          if(response.hits.total > 0) {
         
              //   console.log(response.hits.hits._source)
                          
                     var product_out_main=JSON.parse( JSON.stringify(response.hits.hits) )
                            
                     res.send(product_out_main);

                     console.log(product_out_main)
            }
            else
            {
                res.send("No results found for searchterm: "+req.params.searchterm); 
                }
          
    }
}); 
       
}
else
{
 res.send("error - no searchterm provided.  ") 
    }   
   
})            
                     
// ####################                              
                                                
  
var options = {
      key: fs.readFileSync(settings.https_key),
      cert: fs.readFileSync(settings.https_cert)
};



console.log("++++++++ Listen SSL Mode ++++++++");
var secureServer = https.createServer(options, app);
secureServer.on('error', function (e) {
    // Handle your error here
    console.log(e);
    });
secureServer.listen(settings.port); 
   
   
   console.log("App listening at https://%s:%s", settings.host, settings.port)



