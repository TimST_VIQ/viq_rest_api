// this is configuration file for the VIQ REST API
// Author: Tim Steinlein on 2017-03-28

// configure host and port for middleware
exports.host = 'api.value-iq.com'
exports.port='3000'


// configure user access for rest calls
exports.api_user = 'api_user' 
exports.api_password = 'Salesapp2017' 

// configure https key and cert
exports.https_key='keys/value_iq_api-key.pem'
exports.https_cert='keys/value_iq_api-cert.pem'


// configure MySQL Connection
//exports.elastic_host='localhost'
exports.elastic_host='85.184.249.117'
exports.elastic_port=9200
exports.elastic_user='valueiq'
//exports.elastic_password='valueiq'
exports.elastic_password='Pe6oocem'


// configure the fields to be rezurned by the products API
exports.get_product_fields_included_in_search= ["product_id", "product_name", "brand", "color" ]
