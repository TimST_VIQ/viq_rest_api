var basicAuth = require('express-basic-auth')
 
app.use(basicAuth({
    users: { 'settings.api_user' : 'settings.api_password' },
  //  users: { 'api_user': 'admin'},
    
    unauthorizedResponse: getUnauthorizedResponse,
    challenge: true,
    realm: 'Imb4T3st4pp'
}))
function getUnauthorizedResponse(req) {
    return req.auth ?
        ('Credentials ' + req.auth.user + ':' + req.auth.password + ' rejected') :
        'No credentials provided'
}
